/*
 * sca3300.c
 *
 *  Created on: Oct 26, 2021
 *      Author: Ajmi
 */

#include <sca3300.h>
#include "spi.h"
/**
 * Created on: Oct 26, 2021
 * Last Edited: Oct 26, 2021
 * Author: Ajmi-- ICFOSS
 *
 * @brief      { This function send the SCA3300 init sequence }
 *
 * @return     { true if the device is Successfully configured }
 * @return     { 0  Otherwise}
 */
uint8_t sca3300_init(){
	/* Sensor Powering up Setting time - see startup sequence in datasheet*/
	HAL_Delay(10);
	sendRequest(REQ_WRITE_SW_RESET);
	/*Set Mode*/
	sendRequest(REQ_WRITE_MODE3);
	changeMode(OPMODE3);
	HAL_Delay(14);
	sendRequest ( REQ_READ_STATUS );
	sendRequest ( REQ_READ_STATUS );
	sendRequest ( REQ_READ_WHOAMI );

	uint8_t status = checkChipID();
	return status;
}
/**
 * Created on: Oct 26, 2021
 * Last Edited: Oct 26, 2021
 * Author: Ajmi-- ICFOSS
 *
 * @brief      This function check the SCA3300 WHOAMI register validity.
 *
 * @return     1 if it's a real SCA3300
 */
uint8_t checkChipID(){
	sca3300Frame dummy;

	// Read again to get the correct status data

	sendRequest ( REQ_READ_WHOAMI );
	sendRequest ( REQ_READ_WHOAMI );
	dummy = sendRequest ( REQ_READ_WHOAMI );

	if ( TRUE == dummy.st_IsValid && SCA3300_CHIP_ID == dummy.st_Data )
	{
#ifdef LOG_
		printf("SCA3300 Getting probed successfully.\r\n");
#endif
		/* to avoid first bad measurement */
		sendRequest ( REQ_READ_TEMP );
		return TRUE;
	}
	else
	{
		printf("Failed to probe device!\r\n");
		return FALSE;
	}
}


/*  sendRequest   */
/**
 * Created on: Oct 26, 2021
 * Last Edited: Oct 26, 2021
 * Author: Ajmi-- ICFOSS
 *
 * @brief  Send SPI commands/requests
 *
 * @param  aRequest command in 32bit hex format
 *
 * @retval sca3300Frame spi data frame as received
 *
 **/
sca3300Frame sendRequest(uint32_t aRequest){
	int receiveStatus;
	const int REQUEST_SIZE_BYTES = 4; // u8*4 = 32bits
	uint8_t arryBytes[REQUEST_SIZE_BYTES];
	uint8_t rx[REQUEST_SIZE_BYTES];
	uint32_t responseData = 0x00;

	sca3300Frame cframe;

	arryBytes[3] = (unsigned char) ((aRequest) & 0xFF);
	arryBytes[2] = (unsigned char) ((aRequest >>  8) & 0xFF);
	arryBytes[1] = (unsigned char) ((aRequest >> 16) & 0xFF);
	arryBytes[0] = (unsigned char) ((aRequest >> 24));

	/**************Begin SPI Transmission**************/
	/*Chip Select to Pull down*/
	HAL_GPIO_WritePin(CSB_GPIO_Port, CSB_Pin, GPIO_PIN_RESET);
	/*Transmit the command and receive the data on the falling edge of the clock */
	receiveStatus = HAL_SPI_TransmitReceive(&hspi2, arryBytes, rx, REQUEST_SIZE_BYTES,10);
	/*Chip Select Pull up*/
	HAL_GPIO_WritePin(CSB_GPIO_Port, CSB_Pin, GPIO_PIN_SET);
	/**************END SPI Transmission**************/

	if (receiveStatus != HAL_OK){
		printf("can't send spi message");
		responseData = 0xffffffff;
	}
	else{
	responseData = ((rx[3] & 0xFF) | ((rx[2] << 8) & 0xFF00) | ((rx[1] << 16) & 0xFF0000) | (rx[0] << 24));
	}


	/* Check frame validity = CRC + Return Status */
	cframe.st_ReturnStatus = ( responseData & RS_FIELD_MASK ) >> 24 ;
	cframe.st_IsValid = checkCRCFrame(responseData) && checkRS( cframe.st_ReturnStatus );
	cframe.st_Data = ( responseData & DATA_FIELD_MASK ) >> 8;
	cframe.st_Crc = responseData & CRC_FIELD_MASK;

#ifdef LOG_
	printf("Command : 0x%08x\n\r", aRequest);
	printf("Response Frame : 0x%08x\n\r", responseData);
	printf("response validity: %d\n\r", cframe.st_IsValid);
	printf("response Status:  0x%02x\n\r", cframe.st_ReturnStatus);
	printf("response Data:    0x%04x\n\r", cframe.st_Data);
	printf("response CRC:     0x%02x\n\r\n\r\n\r\n\r", cframe.st_Crc);
#endif
	HAL_Delay(1);
	return cframe;

}

/**
 * Created on: Oct 26, 2021
 * Last Edited: Oct 26, 2021
 * Author: Ajmi-- ICFOSS
 *
 * @brief      Gets the temperature.
 *
 * @param      temp  The temperature
 *
 * @return     1 if the data temperature returns seems correct.
 */

uint8_t getTemperature(uint16_t *temp){
	uint8_t status = FALSE;

    sca3300Frame dummy = sendRequest ( REQ_READ_TEMP );
    dummy = sendRequest ( REQ_READ_TEMP );
    dummy = sendRequest ( REQ_READ_TEMP );

    if ( TRUE == dummy.st_IsValid && ST_ERROR != dummy.st_ReturnStatus)
    {/* Temperature equation in datasheet p.4 */
        *temp = convertTemperature( dummy.st_Data );
#ifdef LOG_
        printf("Temperature: %d°C\r\n", *temp);
#endif
        HAL_Delay(1);
        status  = TRUE;
    }
    else
    {
        *temp = 0;
        status  = FALSE;
        printf("Error during temperature acquisition.\r\n");
    }

    return status;
}
/**
 * Created on: Oct 26, 2021
 * Last Edited: Oct 26, 2021
 * Author: Ajmi-- ICFOSS
 *
 * @brief      Converts raw data from SPI frame in degrees Celsius (°C)
 *
 * @param[in]  aRawTemp  A raw data from SPI frame
 *
 * @return     Temperature in degrees Celsius
 */
uint16_t convertTemperature( const uint16_t rawTemp )
{
	float temp = (( rawTemp / TEMP_SIGNAL_SENSITIVITY )- TEMP_ABSOLUTE_ZERO );

    temp = temp * 100;

    return (uint16_t)temp;
}
/**
 * reated on: Oct 26, 2021
 * Last Edited: Oct 26, 2021
 * Author: Ajmi-- ICFOSS
 *
 * @brief      Read given acceleration from the device
 *
 * @param[in]  aAxe    X,Y,Z axis request
 * @param      aAccel  A acceleration
 *
 * @return     1 success | 0 otherwise.
 */
uint8_t sca3300_GetAccel( const enum accelAxe aAxe, float *aAccel )
{
	uint8_t status = TRUE;
    uint32_t req = 0x00;

    switch(aAxe)
    {
    case ACCEL_X: req = REQ_READ_ACC_X; break;
    case ACCEL_Y: req = REQ_READ_ACC_Y; break;
    case ACCEL_Z: req = REQ_READ_ACC_Z; break;

    default:
        printf("Invalid axe asked. Please Check.\r\n");
        status = FALSE;
        break;
    }

    if (status)
    {
    	sendRequest (req);
        sca3300Frame dummy = sendRequest ( req );

        if ( TRUE == dummy.st_IsValid )
        {
            *aAccel = processAccel( dummy.st_Data, sensivity );
#ifdef LOG_
            printf("Accel[%d]: %fg\r\n", aAxe, *aAccel);
#endif
            status = TRUE;
        }
        else{
#ifdef LOG_
            printf("Invalid response.\r\n");
#endif
            status = FALSE;
        }
    }

    return status;
}
/**
 * Created on: Oct 26, 2021
 * Last Edited: Oct 26, 2021
 * Author: Ajmi-- ICFOSS
 *
 * @brief      Convert data from SPI frame to acceleration
 *
 * @param[in]  aAccel  A data acceleration
 *
 * @return     Acceleration converted (g.)
 */
float processAccel( const uint16_t aAccel, const int aSensivity )
{
	int16_t accelerator;
	accelerator = (int16_t)(aAccel);
	float value = (accelerator / (float)aSensivity);
    return value;
}


/**
 * Created on: Oct 26, 2021
 * Last Edited: Oct 26, 2021
 * Author: Ajmi-- ICFOSS
 *
 * @brief      Check if the CRC of SPI frame is valid
 *
 * @note       verify CRC
 *
 * @param      responseData    the SPI data
 *
 * @return     Return true if CRC is valid , false otherwise
 */

uint8_t checkCRCFrame(uint32_t responseData){
	uint8_t frameCRC = responseData & CRC_FIELD_MASK;
	uint8_t calculatedCRC = calculateCRC(responseData);
	if (frameCRC == calculatedCRC){
		return TRUE;
	}
	else return FALSE;
}

/**
 * Created on: Oct 26, 2021
 * Last Edited: Oct 26, 2021
 * Author: Ajmi-- ICFOSS
 *
 * @brief      Check if the CRC of SPI frame is valid
 *
 * @note       Calculate CRC for 24 MSB's of the 32 bit word
 * 			   (8 LSB's are the CRC field and are not included in CRC calculation)
 *
 * @param      responseData     The pointer
 *
 *
 * @return     Return CRC calculated
 */
uint8_t calculateCRC(uint32_t responseData)
{
  uint8_t bitIndex;
  uint8_t bitValue;
  uint8_t crc;

  crc = 0xFF;
  for (bitIndex = 31; bitIndex > 7; bitIndex--)
  {
    bitValue = (uint8_t)((responseData >> bitIndex) & 0x01);
    crc = bit8_CRC(bitValue, crc);
  }
  crc = (uint8_t)~crc;
  return crc;
}

/**
 * Created on: Oct 26, 2021
 * Last Edited: Oct 26, 2021
 * Author: Ajmi-- ICFOSS
 *
 * @brief      Check if the CRC of SPI frame is valid
 *
 * @note       Calculate CRC for 24 MSB's of the 32 bit word
 * 			   (8 LSB's are the CRC field and are not included in CRC calculation)
 *
 * @param      bitValue     bit value
 *			   crc 			crc
 *
 * @return     Return CRC calculated
 */
static uint8_t bit8_CRC(uint8_t bitValue, uint8_t crc)
{
  uint8_t Temp;

  Temp = (uint8_t)(crc & 0x80);
  if (bitValue == 0x01)
  {
    Temp ^= 0x80;
  }
  crc <<= 1;
  if (Temp > 0)
  {
	  crc ^= 0x1D;
  }
  return crc;
}

/**
 * Created on: Oct 26, 2021
 * Last Edited: Oct 26, 2021
 * Author: Ajmi-- ICFOSS
 * @brief      Change measurement mode
 *
 * @param[in]  aMode  A mode
 *
 * @return     1 if mode is set, 0 for default mode
 */

uint8_t changeMode( const enum operationMode mode)
{/* Return false in default mode */
	uint8_t status = TRUE;

    /* Change mode */
    opMode = mode;

    /* Update sensitivity to get accelerometer */
    switch(mode)
    {
    case OPMODE1:
        sensivity = SENSITIVITY_MODE_1;
        break;

    case OPMODE2:
        sensivity = SENSITIVITY_MODE_2;
        break;

    case OPMODE3:
    case OPMODE4:
        sensivity = SENSITIVITY_MODE_3_4;
        break;

    default:
        printf("Set default sensivity mode.\r\n");
        sensivity = SENSITIVITY_MODE_1;
        opMode = OPMODE1;
        status = FALSE;
        break;
   }

   return status;
}

/**
 * Created on: Oct 26, 2021
 * Last Edited: Oct 26, 2021
 * Author: Ajmi-- ICFOSS
 *
 * @brief      Check Return Code Status of SPI frame
 *
 * @param[in]  aRsCode  A Return Status code
 *
 * @return     True if the RS code is valid, else false
 */

uint8_t checkRS( const uint16_t returnStatus )
{
    if (ST_START_UP == returnStatus || ST_NORMAL_OP == returnStatus )
        return TRUE;
    else
        return FALSE;
}

